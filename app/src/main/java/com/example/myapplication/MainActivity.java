package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView tvValue;
    Button seven, eight, nine, div, c;
    Button mult, four, five, six;
    Button one, two, three, minus, plus;
    Button dot, zero, res;

    String value = "0";
    boolean isOperation = false;
    ArrayList<String> arguments = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvValue = findViewById(R.id.tvValue);
        seven = findViewById(R.id.seven);
        eight = findViewById(R.id.eight);
        nine = findViewById(R.id.nine);
        div = findViewById(R.id.div);
        c = findViewById(R.id.c);
        mult = findViewById(R.id.mult);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        minus = findViewById(R.id.minus);
        plus = findViewById(R.id.plus);
        dot = findViewById(R.id.dot);
        zero = findViewById(R.id.zero);
        res = findViewById(R.id.res);

        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        div.setOnClickListener(this);
        c.setOnClickListener(this);
        mult.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        dot.setOnClickListener(this);
        zero.setOnClickListener(this);
        res.setOnClickListener(this);

        tvValue.setText(value);
    }

    @Override
    public void onClick(View v) {
        Action(((Button)v).getText().toString());
    }
    public void Action(String arg)
    {
        switch (arg)
        {
            case ",": {
                if (!value.contains(",")) value += arg;
                isOperation = false;
            }
                break;
            case "/":
            case "*":
            case "-":
            case "+": {
                    if (isOperation)
                    {
                        arguments.set(arguments.size() - 1, arg);
                    }
                    else
                    {
                        arguments.add(value);
                        arguments.add(arg);
                        value = "0";
                    }
                    isOperation = true;
                }
                break;
            case "=":
                arguments.add(value);
                double tmp = Solve(arguments);
            value = Double.toString(tmp);
            arguments.clear();
            isOperation = false;
            break;
            case "C":
                value = "0";
                isOperation = false;
                break;
            default:
                if (value.startsWith("0") && !value.startsWith("0,")) value = "";
                value += arg;
                isOperation = false;
                break;
        }
        tvValue.setText(value);
    }
    private double Solve(List<String> eq)
    {
        if (eq.size() >= 3)
        {
            int i = 1;
            while (eq.contains("/") || eq.contains("*"))
            {
                double tmp = 0;
                switch (eq.get(i))
                {
                    case "/":
                        tmp = Double.parseDouble(eq.get(i - 1)) / Double.parseDouble(eq.get(i + 1));
                        eq.remove(i - 1);
                        eq.remove(i - 1);
                        eq.set(i - 1, Double.toString(tmp));
                        break;
                    case "*":
                        tmp = Double.parseDouble(eq.get(i - 1)) * Double.parseDouble(eq.get(i + 1));
                        eq.remove(i - 1);
                        eq.remove(i - 1);
                        eq.set(i - 1, Double.toString(tmp));
                        break;
                    default:
                        i = i + 2;
                        break;
                }
            };

            i = 1;
            while (eq.contains("-") || eq.contains("+"))
            {
                double tmp = 0;
                switch (eq.get(i))
                {
                    case "-":
                        tmp = Double.parseDouble(eq.get(i - 1)) - Double.parseDouble(eq.get(i + 1));
                        eq.remove(i - 1);
                        eq.remove(i - 1);
                        eq.set(i - 1, Double.toString(tmp));
                        break;
                    case "+":
                        tmp = Double.parseDouble(eq.get(i - 1)) + Double.parseDouble(eq.get(i + 1));
                        eq.remove(i - 1);
                        eq.remove(i - 1);
                        eq.set(i - 1, Double.toString(tmp));
                        break;
                    default:
                        i = i + 2;
                        break;

                }
            };
        }
        else if (eq.isEmpty())
        {
            return Double.parseDouble(eq.get(0));
        }
        else
        {
            return 0;
        }
        return Double.parseDouble(eq.get(0));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tvValue", tvValue.getText().toString());
        outState.putString("value", value);
        outState.putStringArrayList("arguments", arguments);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        tvValue.setText(savedInstanceState.getString("tvValue"));
        value = (savedInstanceState.getString("value"));
        arguments = savedInstanceState.getStringArrayList("arguments");
    }
}